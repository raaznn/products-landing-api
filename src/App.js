// import './App.css';
import Navbar from "./components/Navbar";
import Products from "./components/Products";

function App() {
  return (
    <div className="App" style={{ height: "100vh" }}>
      <Navbar />
      <Products />
    </div>
  );
}

export default App;
