import React, { Component } from "react";

export default class Navbar extends Component {
  render() {
    return (
      <div className="container-md">
        <nav
          className="navbar navbar-light mx-2 px-4"
          style={{ backgroundColor: "#fcd8a9" }}
        >
          <div className="container-fluid">
            <a className="navbar-brand" href="#">
              <h3>/Products</h3>
            </a>
            <a className="navbar-brand">
              <i class="fas fa-shopping-cart"></i>
            </a>
          </div>
        </nav>
      </div>
    );
  }
}
