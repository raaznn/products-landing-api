import React, { Component } from "react";
import axios from "axios";
import ProductFilter from "./ProductFilter";
import ProductItems from "./ProductItems";

export default class Products extends Component {
  state = {
    loading: true,
    products: [],
    filteredProducts: [],
  };
  componentDidMount() {
    this.fetchProducts();
  }
  fetchProducts = () => {
    this.setState({ loading: true });
    axios
      .get("https://fakestoreapi.com/products/")
      .then((response) => {
        this.setState({
          products: response.data.sort((a, b) => a.price - b.price),
          filteredProducts: response.data.sort((a, b) => a.price - b.price),
          loading: false,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  sortProducts = (e) => {
    const sortBy = e.target.value;
    if (sortBy == "Price(lowest)")
      this.setState({
        filteredProducts: this.state.filteredProducts.sort(
          (a, b) => a.price - b.price
        ),
      });
    else if (sortBy == "Price(highest)")
      this.setState({
        filteredProducts: this.state.filteredProducts.sort(
          (a, b) => b.price - a.price
        ),
      });
    else if (sortBy == "Name(z-a)")
      this.setState({
        filteredProducts: this.state.filteredProducts.sort((a, b) => {
          var productA = a.title.toUpperCase();
          var productB = b.title.toUpperCase();
          return productA > productB ? -1 : 1;
        }),
      });
    else
      this.setState({
        filteredProducts: this.state.filteredProducts.sort((a, b) => {
          var productA = a.title.toUpperCase();
          var productB = b.title.toUpperCase();
          return productA > productB ? 1 : -1;
        }),
      });
  };

  filterCategoryAndRange = (category, range) => {
    this.setState({
      filteredProducts: this.state.products.filter((x) => {
        if ((category == "All" || x.category == category) && x.price < range)
          return true;
        else return false;
      }),
    });
  };

  render() {
    return (
      <div className="container-md px-3 my-3">
        {this.state.loading ? (
          <h3 className="text-center"> Loading Products... </h3>
        ) : (
          <div style={{ height: "90vh" }} className="row m-1">
            <ProductFilter
              products={this.state.products}
              // filterPriceRange={this.filterPriceRange}
              // filterCategory={this.filterCategory}
              filterCategoryAndRange={this.filterCategoryAndRange}
            />
            <ProductItems
              products={this.state.filteredProducts}
              sortProducts={this.sortProducts}
            />
          </div>
        )}
      </div>
    );
  }
}
