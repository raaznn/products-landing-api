import React, { Component } from "react";

export default class ProductFilter extends Component {
  maxPrice = Math.max(...this.props.products.map((x) => x.price));
  productCategories = [...new Set(this.props.products.map((x) => x.category))];

  state = {
    priceRange: this.maxPrice,
    currentCategory: "All",
  };

  updatePriceRange = (e) => {
    this.setState({ priceRange: e.target.value }, () => {
      // console.log(this.state.currentCategory, this.state.priceRange);
      this.props.filterCategoryAndRange(
        this.state.currentCategory,
        this.state.priceRange
      );
    });
  };

  updateCategory = (e) => {
    this.setState(
      {
        currentCategory: e.target.value,
      },
      () => {
        // console.log(this.state.currentCategory, this.state.priceRange);
        this.props.filterCategoryAndRange(
          this.state.currentCategory,
          this.state.priceRange
        );
      }
    );
  };

  getStyle = (cate) => {
    return cate == this.state.currentCategory
      ? { borderBottom: "3px solid brown" }
      : null;
  };

  render() {
    return (
      <div
        className="col-md-3 border rounded p-5 "
        style={{ background: "#fff" }}
      >
        <div className="mx-4">
          <h6>Category</h6>

          <button
            className="btn d-block py-0 m-2 mt-3"
            onClick={this.updateCategory}
            value="All"
            style={this.getStyle("All")}
          >
            All
          </button>
          {this.productCategories.map((x) => (
            <button
              onClick={this.updateCategory}
              value={x}
              className="btn d-block py-0 m-2"
              style={this.getStyle(x)}
            >
              {x}
            </button>
          ))}
        </div>
        <div className="m-4">
          <h6>Price</h6>
          <span className="mx-3">{"Rs. " + this.state.priceRange}</span>
          <input
            onChange={this.updatePriceRange}
            className="mx-3"
            type="range"
            min="0"
            value={this.state.priceRange}
            max={this.maxPrice + 1}
          ></input>
        </div>
        <div className="m-4">
          <button className="btn bg-danger p-1 text-light my-4">
            Clear Filters
          </button>
        </div>
      </div>
    );
  }
}
