import React, { Component } from "react";

export default class ProductItems extends Component {
  render() {
    return (
      <div
        className="col-md-9 border rounded p-5"
        style={{ background: "#fff" }}
      >
        <div className="row">
          <p className="col-2">{this.props.products.length} products found </p>
          <div className="col-md-7 align-middle">
            <hr />
          </div>
          <div className="col-3">
            <span>Sort by: </span>
            <select
              onChange={(e) => this.props.sortProducts(e)}
              className=" border-0"
            >
              <option>Price(lowest)</option>
              <option>Price(highest)</option>
              <option>Name(a-z)</option>
              <option>Name(z-a)</option>
            </select>
          </div>
        </div>

        <div className="row  row-cols-sm-4 gap-3 p-2">
          {this.props.products.map((prod) => {
            return (
              <div className="border rounded text-center  shadow">
                <div className="p-3" style={{ height: "60%" }}>
                  <img
                    style={{ maxWidth: "80%", maxHeight: "80%" }}
                    src={prod.image}
                  ></img>
                </div>
                <div className="">
                  <p className="h-25">{prod.title.slice(0, 40)}</p>
                  <p style={{ color: "brown" }} className="fs-5">
                    {"Rs. " + prod.price}
                  </p>
                  <button
                    style={{ background: "#fcd8a9" }}
                    className="btn mb-3"
                  >
                    {" "}
                    Add to cart
                  </button>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
